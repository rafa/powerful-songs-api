import mongoose from 'mongoose';
import songSchema from '../db/songs';

const Song = mongoose.model('Song', songSchema);

export const getSongs = (req, res) => {
  Song.find({}, (error, songs) => {
    if (error) { res.json(error); }
    res.json(songs);
  });
};

export const getSong = (req, res) => {
  Song.findById(req.params.id, (error, song) => {
    if (error) { res.send(error); }
    res.json(song);
  });
};

export const addNewSong = (req, res) => {
  const newSong = new Song(req.body);
  newSong.save((error, song) => {
    if (error) { res.status(400).json(error); }
    res.json(song);
  });
};
