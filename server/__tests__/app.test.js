import request from 'supertest';
import app from '../app';

describe('Test the root path', () => {
  it('should response 200 with GET', (done) => {
    request(app).get('/').then((response) => {
      expect(response.statusCode).toBe(200);
      done();
    });
  });

  it('should response 404 with POST', (done) => {
    request(app).post('/').then((response) => {
      expect(response.statusCode).toBe(404);
      done();
    });
  });
});
