import request from 'supertest';
import app from '../app';

describe('GET /songs', () => {
  it('should respond with json containing a list of all songs', () => {
    request(app)
      .get('/songs')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);
  });

  it('should respond 200 HTTP code', () => {
    request(app)
      .get('/songs')
      .set('Accept', 'application/json')
      .expect(200);
  });

  it('should respond with 404 error if the url does not match', () => {
    request(app)
      .get('/url-that-does-not-exists')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404);
  });
});

describe('GET /songs/:id', () => {
  it('should respond with json containing a one song', () => {
    request(app)
      .get('/songs/5d1193fceba861838773d7ba')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);
  });

  it('should respond with 200 HTTP code', () => {
    request(app)
      .get('/songs/5d1193fceba861838773d7ba')
      .set('Accept', 'application/json')
      .expect(200);
  });

  it('should respond with 404 error if the song ID it does not exists', () => {
    request(app)
      .get('/url-that-does-not-exists')
      .set('Accept', 'application/json')
      .expect(404);
  });
});


describe('POST /songs', () => {

  const song = {
    songName: 'Easy Tiger',
    songArtist: 'Portugal. The Man',
    songAlbum: 'Woodstock',
    songYear: 2017,
  };

  const badSong = {
    songName: 'Feel It Still',
    songArtist: 'Portugal. The Man',
    songAlbum: 'Woodstock',
  };

  it('should create a song and return a json', () => {
    request(app)
      .get('/songs')
      .send(song)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/);
  });

  it('should return a 200 HTTP code', () => {
    request(app)
      .get('/songs')
      .send(song)
      .set('Accept', 'application/json')
      .expect(200);
  });

  it('should return 400 error if the song schema is bad', () => {
    request(app)
      .get('/songs')
      .send(badSong)
      .set('Accept', 'application/json')
      .expect(400);
  });
});
