import mongoConnection from './db/config';

import app from './app';

app.listen(3000, () => {
  console.log('API is up and running on port 3000');
});
