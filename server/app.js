import { urlencoded, json } from 'body-parser';
import express from 'express';
import router from './routes/songRoutes';

const app = express();

app.get('/', (req, res) => {
  res.json({
    message: 'This is the greatest API for music search',
  });
});

app.use(urlencoded({ extended: true }));
app.use(json());

app.use('/', router);

export default app;
