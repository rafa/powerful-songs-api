import mongoose from 'mongoose';

mongoose.Promise = global.Promise;
const mongoConnection = mongoose.connect(process.env.DB_URL, { useNewUrlParser: true });

mongoConnection
  .then((db) => {
    console.log(
      `Database connected to ${process.env.DB_URL}`,
    );
    return db;
  })
  .catch((err) => {
    if (err.message.code === 'ETIMEDOUT') {
      console.log(`Trying to reconnect to ${process.env.DB_URL}`);
      mongoose.connect(process.env.DB_URL);
    } else {
      console.log(`Can't connect to: ${err}`);
    }
  });

export default mongoConnection;
