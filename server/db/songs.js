import mongoose from 'mongoose';

const { Schema } = mongoose;

const songSchema = new Schema({
  songName: {
    type: String,
    required: 'Song name is required.',
  },
  songArtist: {
    type: String,
    required: 'Song artist is required.',
  },
  songAlbum: {
    type: String,
    required: 'Song album is required.',
  },
  songYear: {
    type: Number,
    required: 'Song year is required.',
  },
});

export default songSchema;
