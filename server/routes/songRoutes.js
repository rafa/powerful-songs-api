import express from 'express';
import { getSongs, addNewSong, getSong } from '../controller/songController';

const router = express.Router();

router.route('/songs')
  .get(getSongs)
  .post(addNewSong);

router.route('/songs/:id')
  .get(getSong);

export default router;
