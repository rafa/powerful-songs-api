# Powerful Songs API

This is a simpleREST  API created with [NodeJS](https://nodejs.org/en/) and it's using [Express](https://expressjs.com/) as web framework and [MongoDB](https://www.mongodb.com/) as database.

## Installation

It's very simple to install, you just need NodeJS and NPM in your computer and use the command
`npm install` this is going to install all the required dependencies in a folder called `node modules`.

I'm assuming that you have already a MongoDB database running, if it's not the case, you can go [here](https://docs.mongodb.com/manual/introduction/) and learn a little bit of MongoDB as database and learn how to create and run a DB.

You need to create a file to store your secrets, such as the DB URL with the user and password, that's why you need to make a copy of the .env.example file with this command:
`mv .env.example .env`
and then you need to replace the fake URL that shows the file with your MongoDB URL

You also need to compile the project, that's because it's written with ES6. To do that just run:
`npm run compile`
this will send a compiled version of the project into a folder called `lib/`

To run the project you just need to use this command:
`npm start`
and that's all, you can go to your browser and take a look at [http://localhost:3000](http://localhost:3000) 🚀

![Root API](./readme_images/getRoot.png)

## About the API

The API has 3 main endpoints

 1. `/songs` [GET]: Returns all the songs in the database
![Using Insomnia to test the GET in /songs](./readme_images/getSongs.png)
 2. `/songs` [POST]: Adds one song to the database
![Using Insomnia to test the POST in /songs](./readme_images/postSong.png)
 3. `/songs/:id` [GET]: Returns an specific song from the database
![Using Insomnia to test the GET in /songs/:id](./readme_images/getSong.png)

To make a POST request you need to send it to `/songs` and the json with the songs has to have the following strcuture:

```javascript
{
  "songName": "The Two of Us",
  "songArtist": "The Jesus and the Mary Chain",
  "songAlbum": "Damage and Joy",
  "songYear": 2017
}
```

All the fields are required and if you don't send a field it'd let you create a new song.

Have fun 🤖

## Testing

This project has unit tests using [
](https://jestjs.io/en/) as the test framework in the `__tests__` folder, you can add yours there!
To run the test and make sure that everything is ok you need to use the command:
`npm test`

## Contributors

- 🤓Rafa Miranda - rafayotuel@gmail.com
